#!/usr/bin/node
'use strict';

const ESC       = '\x1b[';
const fs        = require("fs");
const delivery  = require("delivery");
const config    = require("./config");
const conf      = config[ config.checked || 'yandex' ];
const { print } = require('./modules/print_methods');

const rows = [];
process.stdin.on('data', function(d){
	rows.push(d.toString());
});

const email = process.argv[2] || '';
const subj = process.argv[3] || '';
const name = process.argv[4] || '';
process.stdin.on('close', function(d){
	const body = rows.join('');
	print( 'Sending To: ', 0);
	if(email){
		print( email, 0 )
		print ( ' From: ', 0);
		if( name ) print( name + ' ', 0);
		print( '<' + conf.user + '> ', 0)
		if( subj ) print( ' Subj: ' + subj, 0);
		print( ' ... ', 0 );
		delivery.mail.send({to: email, subj, name, text: body || ''}, conf, 2,
			function(result){
				print( ' [ ', 0 );
				print( ESC + '32m'  + 'ok' + ESC + '0m', 0 );
				print( ' ] ' );
				process.exit(0);
			},
			function(err){
				print( ' [ ', 0 );
				print( ESC + '31m' + 'ERR: ' + ESC + '0m' + ESC + '4m' + err.message + ESC + '0m', 0);
				print( ' ] ' );
				process.stderr.write( err.message );
				print('');
				process.exit(1);
			}
		);
	}else{
		print( ' [ ', 0 );
		print('ERR: ' + 'Пустой адрес', 0);
		print( ' ] ' );
		process.exit(1);
	};
});
